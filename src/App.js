import React from 'react';
import './App.css';

// Organisms
import Navbar from './components/organisms/Navbar/Navbar';
import Header from './components/organisms/Header/Header';
import Home from './components/templates/Home/Home';

function App() {
  return (
    <div>
      <Home />
    </div>
  );
}

export default App;
