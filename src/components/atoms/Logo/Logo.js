import React from 'react';
import PropTypes from 'prop-types';

require('./Logo.css');

const Logo = props => (
    <img className="logo" src={props.image} />
)

Logo.propTypes = {
    image: PropTypes.string.isRequired
}

export default Logo;