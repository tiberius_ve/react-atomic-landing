import React from 'react';
import PropTypes from 'prop-types';

require('./Title.css');

const Title = props => (
<h1 className={`${props.class}`}>{props.title}</h1>
)

Title.propTypes = {
    title: PropTypes.string.isRequired,
    class: PropTypes.string,
}

export default Title;