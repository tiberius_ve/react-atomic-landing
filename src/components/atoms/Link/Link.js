import React from 'react';
import PropTypes from 'prop-types';

require('./Link.css');

const Link = props => (
    <li><a href={props.href} className="link">{props.text}</a></li>
)

Link.propTypes = {
    href: PropTypes.string.isRequired,
    text: PropTypes.string
}

export default Link;