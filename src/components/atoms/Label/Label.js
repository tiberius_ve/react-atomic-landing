import React from 'react';
import PropTypes from 'prop-types';

require('./Label.css')

const Label = props => (
<label>{props.label}</label>
)

Label.propTypes = {
    label: PropTypes.string.isRequired
}

export default Label;