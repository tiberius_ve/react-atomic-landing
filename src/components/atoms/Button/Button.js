import React from 'react';
import PropTypes from 'prop-types';

require('./Button.css')

const Button = props => (
    <button className={`btn ${props.class}`}>{props.text}</button>
)

Button.propTypes = {
    text: PropTypes.string.isRequired,
    class: PropTypes.string
}

export default Button;