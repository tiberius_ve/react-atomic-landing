import React from 'react';

// Atoms
import Button from '../../atoms/Button/Button'

require('./Account.css');

const Account = props => (
    <div className="account_el">
        <Button text="Login" class="primary" />
        <Button text="Sign Up" class="secondary" />
    </div>
)

export default Account;