import React from 'react';

// Atoms
import Link from '../../atoms/Link/Link';

require('./Navigation.css');

const navs = [
    { id: 1, text: "About", href: "#" },
    { id: 2, text: "Products", href: "#" },
    { id: 3, text: "Pricing", href: "#" },
    { id: 4, text: "Blog", href: "#" },
    { id: 5, text: "Contact", href: "#" }
]

const Navigation = props => (
    <ul className="navbar_ul">
        {navs.map((nav) => {
            return (
                <Link href={nav.href} text={nav.text} />
            )
        })}
    </ul>
)

export default Navigation;