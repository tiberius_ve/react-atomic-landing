import React from 'react';
import PropTypes from 'prop-types';

// Atoms 
import Link from '../../atoms/Link/Link'

require('./Footer_Link.css');

const Footer_Link = props => (
    <ul>
        <Link href={props.href} text={props.text}/>
    </ul>
)

Footer_Link.propTypes = {
    href: PropTypes.string,
    text: PropTypes.string.isRequired
}

export default Link;