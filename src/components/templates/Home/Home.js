import React from 'react';

// Organisms
import Navbar from '../../organisms/Navbar/Navbar';
import Header from '../../organisms/Header/Header';
import Description from '../../organisms/Description/Description';
import Info from '../../organisms/Info/Info';
import Utilities from '../../organisms/Utilities/Utilities';
import Footer from '../../organisms/Footer/Footer';

require('./Home.css');

const utilities = [{ 
        pretitle: 'Foreign Exchange Control', 
        title: 'Control of currency changes', 
        content: 'This module is responsible for tracking the exchange rate, it is updated as soon as it receives new data and is synchronized with the table. You can independently indicate which courses you want to see on your page, just add them to the table', 
        image: 'image2.png' 
    },
    { 
        pretitle: 'Technical Support', 
        title: 'Solves your issues instantly', 
        content: 'The technical support of our service is working at a completely new level, we are trying to solve your problems instantly, you just need to describe your problem and we immediately pounce for its solution. Our technical assistants work around the clock', 
        image: 'image3.png'  
    },
    { 
        pretitle: 'Foreign Exchange Control', 
        title: 'Track your transactions', 
        content: 'Tracking purchases always allows you to be aware of where your money is, you are always aware of how much money you spent and how much you have on the card, so you can control all your transactions from one card', 
        image: 'image4.png'  
    }
]

const footer = [{ 
        title: 'Company', 
        content: ['About us', 'Career', 'Support'] 
    },
    { 
        title: 'Product', 
        content: ['Features', 'Pricing', 'Login'] 
    },
    { 
        title: 'Resources', 
        content: ['Help', 'Blog', 'Status'] 
    },
    { 
        title: 'Performance', 
        content: ['Media', 'Reach', 'Creative'] 
    },
    { 
        title: 'Contact us', 
        content: ['+ 44 (0) 77 2123 9241', 'hello@onibluesocial.com'],
        social: true 
    }
]

const Home = props => (
    <div className="home">
        <header>
            <Navbar />
        </header>
        <main id="main">
            <section className="header">
                <Header />
            </section>
            <section className="description">
                <Description 
                    title="Idea of the Project"
                    content="We have developed a system that allows you to unite many bank cards into one electronic card, thereby tracking all data synchronizing all of them as one whole, you can at any time track transactions of several cards simultaneously and this is only a small part of what Oniblue can."
                />
            </section>
            <section className="info">
                <Info />
            </section>
            <section className="utilities">
                <Utilities elements={utilities} />
            </section>
            <section className="description colored">
                <Description 
                    title="Start monitoring your cards right now"
                    content="If you register now, you will receive 15 days of product use completely unheeded"
                    interaction={true}
                />
            </section>
            <section className="footer">
                <Footer elements={footer}/>
            </section>
        </main>
    </div>
)

export default Home;