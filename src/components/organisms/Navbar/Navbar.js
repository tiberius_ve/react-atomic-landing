import React from 'react';
import logo from '../../../logo.png';

// Bootstrap Grid
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

// Atoms
import Logo from '../../atoms/Logo/Logo';

// Molecules
import Navigation from '../../molecules/Navigation/Navigation';
import Account from '../../molecules/Account/Account';

require('./Navbar.css');

const Navbar = props => (
    <nav>
        <Container>
            <Row>
                <Col xl={9} xs={12} className="nav_col">
                    <Col xl={3} xs={12}>
                        <Logo image={logo} />
                    </Col>
                    <Col xl={9} xs={12}>
                        <Navigation />
                    </Col>
                </Col>
                <Col xl={3} xs={12} className="acc_col d-none d-xl-block">
                    <Account />
                </Col>
            </Row>
            <Row className="d-xl-none">
               <Account />
            </Row>
        </Container>
    </nav>
)

export default Navbar;