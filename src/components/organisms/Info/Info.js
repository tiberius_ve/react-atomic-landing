import React from 'react';
import PropTypes from 'prop-types';

// Bootstrap Grid
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

// Atoms
import Title from '../../atoms/Title/Title';

require('./Info.css');

const Info = props => (
    <Container>
        <Row>
            <Col xs={12} lg={4}>
                <img src={process.env.PUBLIC_URL + '/icon1.png'}></img>
                <Title title="Smart electronic card" class="thumb-title" color="dark"/>
                <p className="info_p">Our map is designed in such a way that it unites all your cards in the system and synchronizes data as a multifunction card.</p>
            </Col>
            <Col xs={12} lg={4}>
                <img src={process.env.PUBLIC_URL + '/icon2.png'}></img>
                <Title title="Real-time statistics" class="thumb-title" color="dark"/>
                <p className="info_p">Our map is designed in such a way that it unites all your cards in the system and synchronizes data as a multifunction card.</p>
            </Col>
            <Col xs={12} lg={4}>
                <img src={process.env.PUBLIC_URL + '/icon3.png'}></img> 
                <Title title="Receiving the information" class="thumb-title" color="dark"/>
                <p className="info_p">Our map is designed in such a way that it unites all your cards in the system and synchronizes data as a multifunction card.</p>
            </Col>
        </Row>
    </Container>
)

export default Info;