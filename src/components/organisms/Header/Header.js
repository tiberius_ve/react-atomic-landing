import React from 'react';

// Bootstrap Grid
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

// Atoms
import Title from '../../atoms/Title/Title';
import Button from '../../atoms/Button/Button'

require('./Header.css');

const Header = props => (
    <Container fluid>
        <Row>
            <Col>
                <Col xs={12} lg={{ span:7, offset:2 }}>
                    <Title title="Combine all your credit cards in one place" class="header-title" color="white"/>
                    <p>We allow you to connect different bank cards in one system, in which you will have the opportunity to manage your financial data and track the statistics of your costs</p>
                    <Button text="Get started" class="terciary" />
                </Col>
            </Col>
            <Col className="d-none d-lg-block"><img className="interface" src={process.env.PUBLIC_URL + '/image1.png'}></img></Col>
        </Row>
        <Row>
            <span className="circle"></span>
        </Row>
    </Container>
)

export default Header;