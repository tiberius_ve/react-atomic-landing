import React from 'react';
import PropTypes from 'prop-types';

// Bootstrap Grid
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

// Atoms
import Link from '../../atoms/Link/Link';

require('./Footer.css');

const Footer = props => (
    <Container>
        <Row>
            {props.elements.map((element, i) => 
                <Col xs={6} lg>
                    <ul>
                        <h2>{element.title}</h2>
                        {element.content.map((cont, i) => 
                        <Link href="#" text={cont} />
                        )}
                        {element.social && 
                        <img src={process.env.PUBLIC_URL + 'social.png'} />
                        }
                    </ul>
                </Col>    
            )}
        </Row>
        <Row>
            <Col>
                <img src={process.env.PUBLIC_URL + 'logo_color.png'} />
                <h4>All rights reserved Oniblue™ 2018</h4>
            </Col>
        </Row>
    </Container>
)

Footer.propTypes = {
    title: PropTypes.object,
    content: PropTypes.object
}

export default Footer;