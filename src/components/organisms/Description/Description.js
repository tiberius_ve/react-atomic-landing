import React from 'react';
import PropTypes from 'prop-types';

// Bootstrap Grid
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

//Atoms
import Title from '../../atoms/Title/Title';
import Button from '../../atoms/Button/Button'

require('./Description.css');

const Description = props => (
    <Container>
        <Row>
            {!props.interaction && (
                <Col xs={12}>
                    <img src={process.env.PUBLIC_URL + 'dotted-vertical.png'} />
                </Col>
            )}
            <Col xs={12}>
                <Title title={props.title} class="description-title"/>
                <p>{props.content}</p>
            </Col>
        </Row>
        {props.interaction && (
        <Row>
            <Col>
                <Button text="Get Started Free" class="terciary" />
            </Col>
            <Col>
                <p>or you can view pricing</p>
            </Col>
        </Row>
        )}
    </Container>
)

Description.propTypes = {
    title: PropTypes.string.isRequired,
    content: PropTypes.string.isRequired,
    interaction: PropTypes.bool
}

export default Description;