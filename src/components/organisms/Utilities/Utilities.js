import React from 'react';
import PropTypes from 'prop-types';

// Bootstrap Grid
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

// Atoms
import Label from '../../atoms/Label/Label';
import Title from '../../atoms/Title/Title'
import Button from '../../atoms/Button/Button';

require ('./Utilities.css');

const Utilities = props => (
    <Container>
        {props.elements.map((element,i) => 
            <Row>
                <Col xs={12} lg={6}>
                    <Label label={element.pretitle} />
                    <Title title={element.title} class="description-title" color="dark"/>
                    <p class="info_p">{element.content}</p>
                    <Button text="Learn more" class="quaternary"/>
                </Col>
                <Col xs={12} lg={6}>
                    <img src={process.env.PUBLIC_URL + element.image} />
                </Col>
            </Row>
        )}
    </Container>
)

Utilities.propTypes = {
    pretitle: PropTypes.object,
    title: PropTypes.object,
    content: PropTypes.object
}

export default Utilities;